import math
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

def computeAccuracy(  y_pred_label: np.ndarray, 
                      y_label: np.ndarray,
                      n_labels: np.int64
                   ) :
    pcorrect = np.mean(y_pred_label == y_label)
    conf_mat = np.zeros(shape=(n_labels,n_labels))
    for i in range(n_labels):
        indices_i = np.squeeze(np.array(np.where(y_label == i)))
        label_pred_i = y_pred_label[indices_i]
        for j in range(n_labels):
            indices_label_pred_ij = np.squeeze(np.array(np.where(label_pred_i == j)))
            conf_mat[i,j] = indices_label_pred_ij.size / indices_i.size

    return pcorrect, conf_mat

def encodeLabels( label_arr: np.ndarray, 
                  num_labels: np.int64
                ) -> np.ndarray :
    data = tf.keras.utils.to_categorical( label_arr,
                                          num_classes = num_labels,
                                          dtype = 'float32')
    return data

# Deep feedforward classifier
def classifyDense(  x_train: np.ndarray,
                    labels_train: np.ndarray,
                    x_test: np.ndarray,
                    labels_test: np.ndarray,
                    num_neurons: list,
                    f_a: list,
                    lr: np.float64,
                    iters: np.int64,
                    batch_frac: np.float64
                 ) :

    n_classes = len(np.unique(labels_train))
    y_train = encodeLabels( labels_train, n_classes )
    y_test = encodeLabels( labels_test, n_classes )

    batch_size = int(batch_frac*x_train.shape[0])

    n_layers = len(num_neurons)
    model = keras.Sequential()
    model.add( layers.Input(shape=(x_train.shape[1],)) )
    for i in range(n_layers):
        model.add( layers.Dense(num_neurons[i], activation = f_a[i]) )
    model.add( layers.Dense(y_train.shape[1]) )
    model.add( layers.Softmax() )

    optim = tf.keras.optimizers.Adam(learning_rate = lr)
    loss_fn = tf.keras.losses.CategoricalCrossentropy()

    model.compile(
        optimizer = optim,  # Optimizer
        loss = loss_fn, # Loss function
        metrics = ['accuracy'],
    )
    history = model.fit(
      x_train, y_train,
      epochs = iters,
      batch_size = batch_size,
      validation_split = 0,
      verbose = 0, 
      validation_data = (x_test,y_test),
      shuffle = True
    )

    cross_entropy = tf.keras.losses.CategoricalCrossentropy()
    train_loss = cross_entropy(y_train,model(x_train)).numpy()
    print("  -->modClassifier.classify(): training loss=", 
          str(np.round(train_loss, 6)) )

    y_train_preds = model(x_train).numpy()
    labels_train_pred = np.argmax(y_train_preds, axis = 1)
    train_accuracy, train_conf_mat = computeAccuracy( labels_train_pred, 
                                                      labels_train,
                                                      n_classes )
    print("  -->modClassifier.classify(): training accuracy is", 
          str(np.round(train_accuracy, 4)) )

    y_test_preds = model(x_test).numpy()
    labels_test_pred = np.argmax(y_test_preds, axis = 1)
    test_accuracy, test_conf_mat = computeAccuracy( labels_test_pred, 
                                                    labels_test,
                                                    n_classes )
    print("  -->modClassifier.classify(): test accuracy is", 
          str(np.round(test_accuracy, 4)) )

    return [  model, 
              history, 
              labels_train_pred, 
              labels_test_pred, 
              train_conf_mat, 
              test_conf_mat ]

# Deep convolutional classifier
def classifyConv( x_train: np.ndarray,
                  labels_train: np.ndarray,
                  x_test: np.ndarray,
                  labels_test: np.ndarray,
                  conv_dim: np.int64,
                  n_filters_cnn: list,
                  k_cnn: tuple,
                  f_a_cnn: list,
                  m_cnn: tuple,
                  num_neurons_dnn: list,
                  f_a_dnn: list,
                  lr: np.float64,
                  iters: np.int64,
                  batch_frac: np.float64
                ) :

    n_classes = len(np.unique(labels_train))
    y_train = encodeLabels( labels_train, n_classes )
    y_test = encodeLabels( labels_test, n_classes )

    batch_size = int(batch_frac*x_train.shape[0])

    nd = x_train.ndim-1
    if (nd == 2):
      x1 = x_train[1:,:,:]
    elif (nd == 3):
      x1 = x_train[1,:,:,:]
    else:
      print("ERROR: X_train dimensions must be 3 or 4!")
      print("ERROR: X_train size: ", x_train.shape );

    # initialize
    model = keras.Sequential()

    # add convolutional layers
    n_layers_cnn = len(n_filters_cnn)
    for i in range(n_layers_cnn):
      if (conv_dim == 2):
        model.add( layers.Conv2D( n_filters_cnn[i], 
                                  k_cnn, 
                                  activation = f_a_cnn[i],
                                  input_shape = (x1.shape) ) )
        model.add( layers.MaxPooling2D( m_cnn ) )
      elif (conv_dim == 3):
        model.add( layers.Conv3D( n_filters_cnn[i], 
                                  k_cnn, 
                                  activation = f_a_cnn[i],
                                  input_shape = (x1.shape) ) )
        model.add( layers.MaxPooling3D( m_cnn ) )
      else:
        print("ERROR: higher dimensions not implemented")

    # add dense layers
    model.add( layers.Flatten() )
    n_layers_dnn = len(num_neurons_dnn)
    for i in range(n_layers_dnn):
        model.add( layers.Dense(num_neurons_dnn[i], activation = f_a_dnn[i]) )
    model.add( layers.Dense(y_train.shape[1]) )
    model.add( layers.Softmax() )

    optim = tf.keras.optimizers.Adam(learning_rate = lr)
    loss_fn = tf.keras.losses.CategoricalCrossentropy()

    model.compile(
        optimizer = optim,  # Optimizer
        loss = loss_fn, # Loss function
        metrics = ['accuracy'],
    )

    train_loss = loss_fn(y_train,model(x_train)).numpy()
    print("  -->modClassifier.classify(): initial training loss=", 
          str(np.round(train_loss, 6)) )

    history = model.fit(
      x_train, y_train,
      epochs = iters,
      batch_size = batch_size,
      validation_split = 0,
      verbose = 0, 
      validation_data = (x_test,y_test),
      shuffle = True
    )

    train_loss = loss_fn(y_train,model(x_train)).numpy()
    print("  -->modClassifier.classify(): training loss=", 
          str(np.round(train_loss, 6)) )

    y_train_preds = model(x_train).numpy()
    labels_train_pred = np.argmax(y_train_preds, axis = 1)
    train_accuracy, train_conf_mat = computeAccuracy( labels_train_pred, 
                                                      labels_train,
                                                      n_classes )
    print("  -->modClassifier.classify(): training accuracy is", 
          str(np.round(train_accuracy, 4)) )

    y_test_preds = model(x_test).numpy()
    labels_test_pred = np.argmax(y_test_preds, axis = 1)
    test_accuracy, test_conf_mat = computeAccuracy( labels_test_pred, 
                                                    labels_test,
                                                    n_classes )
    print("  -->modClassifier.classify(): test accuracy is", 
          str(np.round(test_accuracy, 4)) )

    return [  model, 
              history, 
              labels_train_pred, 
              labels_test_pred, 
              train_conf_mat, 
              test_conf_mat ]

# Train an already-initialized model
def classifyModel(  model: tf.keras.Model,
                    x_train: np.ndarray,
                    labels_train: np.ndarray,
                    x_test: np.ndarray,
                    labels_test: np.ndarray,
                    lr: np.float64,
                    iters: np.int64,
                    batch_frac: np.float64,
                 ) :

    n_classes = len(np.unique(labels_train))
    y_train = encodeLabels( labels_train, n_classes )
    y_test = encodeLabels( labels_test, n_classes )

    batch_size = int(batch_frac*x_train.shape[0])

    optim = tf.keras.optimizers.Adam(learning_rate = lr)
    loss_fn = tf.keras.losses.CategoricalCrossentropy()

    model.compile(
        optimizer = optim,  # Optimizer
        loss = loss_fn, # Loss function
        metrics = ['accuracy'],
    )

    train_loss = loss_fn(y_train,model(x_train)).numpy()
    print("  -->modClassifier.classify(): initial training loss=", 
          str(np.round(train_loss, 6)) )

    history = model.fit(
      x_train, y_train,
      epochs = iters,
      batch_size = batch_size,
      validation_split = 0,
      verbose = 0, 
      validation_data = (x_test,y_test),
      shuffle = True
    )

    train_loss = loss_fn(y_train,model(x_train)).numpy()
    print("  -->modClassifier.classify(): training loss=", 
          str(np.round(train_loss, 6)) )

    y_train_preds = model(x_train).numpy()
    labels_train_pred = np.argmax(y_train_preds, axis = 1)
    train_accuracy, train_conf_mat = computeAccuracy( labels_train_pred, 
                                                      labels_train,
                                                      n_classes )
    print("  -->modClassifier.classify(): training accuracy is", 
          str(np.round(train_accuracy, 4)) )

    y_test_preds = model(x_test).numpy()
    labels_test_pred = np.argmax(y_test_preds, axis = 1)
    test_accuracy, test_conf_mat = computeAccuracy( labels_test_pred, 
                                                    labels_test,
                                                    n_classes )
    print("  -->modClassifier.classify(): test accuracy is", 
          str(np.round(test_accuracy, 4)) )

    return [  model, 
              history, 
              labels_train_pred, 
              labels_test_pred, 
              train_conf_mat, 
              test_conf_mat ]

# Wrapper for all the classify functions
def classify(*args):
  if len(args) == 9 and isinstance(args[0], np.ndarray) :
    return classifyDense(*args)
  elif len(args) == 14 and isinstance(args[0], np.ndarray) :
    return classifyConv(*args)
  elif len(args) == 8 and isinstance(args[0], tf.keras.Model) :
    return classifyModel(*args)
  else :
    raise TypeError("invalid call to classify()!")
